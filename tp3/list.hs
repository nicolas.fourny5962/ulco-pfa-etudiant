
data List a = Nil | Cons a (List a)

sumList :: Num a => List a -> a
sumList Nil = 0
sumList (Cons elm tail) = elm + (sumList tail)

flatList :: (List String) -> String 
flatList Nil = ""
flatList (Cons elm tail) = elm ++ (flatList tail)

toHaskell :: (List a) -> [a]
toHaskell Nil = []
toHaskell (Cons elm tail) = elm:(toHaskell tail)

fromHaskell :: [a] -> (List a)
fromHaskell l = 

myShowList :: Show a => List a -> String
myShowList Nil = ""
myShowList (Cons elm tail) = show elm ++ " " ++ myShowList tail

main :: IO()
main = do 
  let l1 = Nil
  let l2 = Cons 2 Nil
  let l3 = Cons 1 (Cons 2 Nil)

  print $ sumList l1
  print $ sumList l2
  print $ sumList l3
  print $ toHaskell l3