{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple


selectAllArtist:: Connection -> IO [(Int,Text)]
selectAllArtist conn = query_ conn "SELECT * FROM artist"

selectTitre :: Connection -> IO [(Text,Text)]
selectTitre conn = query_ conn 
    "SELECT artist_name, title_name FROM title \
    \INNER JOIN artist ON title_artist = artist_id"

main :: IO ()
main = do withConnection "music.db" selectAllArtist >>= mapM_ print
          withConnection "music.db" selectTitre >>= mapM_ print