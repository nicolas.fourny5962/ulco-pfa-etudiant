{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module View where

import Model
import Lucid
import Data.Text as T
import Data.Text.Lazy as L

indexPage :: [Rider] -> L.Text
indexPage riders = mkPage "ptivelo" $ do
    mapM_ mkRider riders

mkRider :: Rider -> Html ()
mkRider r = div_ [class_ "row"] $ do
    h2_ [class_ "col-md-12"] $ toHtml $ _name r
    mapM_ mkImg (_imgs r)

mkImg :: String -> Html ()
mkImg i = div_ [class_ "col-md-6 text-center"] $ do
    img_ [src_ (T.pack i), class_ "w-50"]
    p_ $ toHtml $ i


bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

mkPage :: L.Text -> Html () -> L.Text
mkPage myTitle myHtml = renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ $ toHtml myTitle
        body_ [class_ "bg-warning container"] $ do
            p_ [class_ "h1 border-bottom border-dark pb-4"] "ptivelo"
            div_ [class_ ""] myHtml
