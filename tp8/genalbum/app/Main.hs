import Site
import Lucid

main :: IO ()
main = do 
    sitesE <- loadSites "data/genalbum.json"
    case sitesE of
        Left err -> putStrLn err
        Right sites -> print sites
    
    renderToFile "index.html" myHtml


