{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import GHC.Generics
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person where
    toJSON (Person firstname lastname birthyear) =
        object ["first" .= firstname, "birth" .= birthyear, "last" .= lastname]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO()
main = 
    encodeFile "out-aeson2.json" person
