{-# LANGUAGE OverloadedStrings #-}

import Lucid

myHtml :: Html ()
myHtml = do
    doctype
    html $ do
        head_ $ meta_ [charset_ "utf8"]
        body $ do
            h1 "hello"
            img [src "toto.png"]
            p $ do
                "this is"
                a [href_ "toto.png"] "a link"

main :: IO()
main = renderToFile "out-lucid2.html" myHtml
