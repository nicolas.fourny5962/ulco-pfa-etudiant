-- create database:
-- sqlite3 ulcoforum.db < ulcoforum.sql

CREATE TABLE thread (
  thread_id INTEGER PRIMARY KEY AUTOINCREMENT,
  thread_title TEXT
);

CREATE TABLE msg (
  msg_id INTEGER PRIMARY KEY AUTOINCREMENT,
  msg_author TEXT, 
  msg_text TEXT, 
  msg_thread INTEGER, 
  FOREIGN KEY(msg_thread) REFERENCES thread(thread_id)
);

INSERT INTO thread VALUES(1, 'Vacances 2020-2021');
INSERT INTO thread VALUES(2, 'Master Info nouveau programme');

INSERT INTO msg VALUES(1, 'John Doe', "Pas de vacances cette année", 1);
INSERT INTO msg VALUES(2, 'Toto', "Youpi", 1);
INSERT INTO msg VALUES(3, 'Toto', "Tous les cours passent en Haskell", 2);


