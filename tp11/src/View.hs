{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module View where

import Model
import Lucid
import Data.Text as T
import Data.Text.Lazy as L
import TextShow

bootstrap :: T.Text
bootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

indexPage :: Html ()
indexPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ "ulcoforum"
        body_ [class_ "container"] $ do
            a_ [href_ "/", class_ "h1 text-primary"] "ulcoforum"
            li_ [class_ "d-flex border-bottom border-gray pb-1"] $ do
                a_ [href_ "alldata"] "alldata"
                p_ " - "
                a_ [href_ "allthreads"] "allthreads"
            p_ [class_ "pt-3"] "This is ulcoforum"

allDataPage :: [Msg] -> Html ()
allDataPage msgs = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ "ulcoforum"
        body_ [class_ "container"] $ do
            a_ [href_ "/", class_ "h1 text-primary"] "ulcoforum"
            li_ [class_ "d-flex border-bottom border-gray pb-1"] $ do
                a_ [href_ "alldata"] "alldata"
                p_ " - "
                a_ [href_ "allthreads"] "allthreads"
            p_ [class_ "p-3"] "All data:"
            ul_ $ mapM_ viewMsg msgs


viewMsg :: Msg -> Html ()
viewMsg (Msg author text thread) = li_ $ do
    strong_ $ toHtml $ thread
    div_ $ do
        toHtml author
        ": "
        toHtml text

allThreadsPage :: [Thread] -> Html ()
allThreadsPage threads = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            link_ [ rel_ "stylesheet", href_ bootstrap]
            title_ "ulcoforum"
        body_ [class_ "container"] $ do
            a_ [href_ "/", class_ "h1 text-primary"] "ulcoforum"
            li_ [class_ "d-flex border-bottom border-gray pb-1"] $ do
                a_ [href_ "alldata"] "alldata"
                p_ " - "
                a_ [href_ "allthreads"] "allthreads"
            p_ [class_ "p-3"] "All threads:"
            ul_ $ mapM_ viewThreads threads

viewThreads :: Thread -> Html ()
viewThreads (Thread ind title ) = li_ $ do
    a_ [href_ (showt ind)] $ toHtml $ title
    