import qualified Data.Text as T
import qualified Data.Text.IO as T
main :: IO()
main = do
    --lire le texte1.hs en ByteString
    file <- T.readFile "text1.hs"
    --convertir en string
    let contents = T.unpack file
    --afficher
    putStrLn contents
