import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as T
import qualified Data.ByteString.Char8 as C

main :: IO()
main = do
    file <- T.readFile "text4.hs"
    let contents = E.encodeUtf8 file
    C.putStr contents
