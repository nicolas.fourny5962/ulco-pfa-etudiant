import qualified Data.Text.Lazy.IO as L
import qualified Data.Text.Lazy as L
import qualified Data.Text.IO as T

main :: IO()
main = do
    file <- T.readFile "text5.hs"
    let contents = L.fromStrict file
    L.putStr contents
